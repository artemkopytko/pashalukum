<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package pashalukum
 */

$PATH = '/pashalukum/wp-content/themes/pashalukum'

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

    <link href="https://fonts.googleapis.com/css?family=Playfair+Display&amp;subset=cyrillic" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400&amp;subset=cyrillic" rel="stylesheet">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <div class="wrapper">
        <div class="content">
            <nav class="navigation">
                <ul class="nav-links container">
                    <li>
                        <a href="#" class="no-hover"><img src="<?=$PATH;?>/images/logo.png" alt=""></a></li>
                    <li>
                        <a href="#">Продукция</a>
                    </li>
                    <li>
                        <a href="#">Производство</a>
                    </li>
                    <li>
                        <a href="#">Упаковка</a>
                    </li>
                    <li>
                        <a href="#">Сделать Заказ</a>
                    </li>
                    <li>
                        <a href="#">Сертификаты</a>
                    </li>
                    <li>
                        <a href="#">Новинки</a>
                    </li>
                    <li>
                        <a href="#">Контакты</a>
                    </li>
                </ul>
            </nav>

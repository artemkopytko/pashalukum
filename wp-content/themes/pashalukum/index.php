<?php /* Template Name: home */

$PATH = '/pashalukum/wp-content/themes/pashalukum'

?>

<?php get_header();?>


    <style>
        .intro {
            background-image: url("<?=$PATH;?>/images/intro-bg-image.jpg");;
        }
        .cooperate-with-us {
            background-image: url("<?=$PATH;?>/images/IMG_7090.jpg");
        }
        .leaders {
            background-image: url("<?=$PATH;?>/images/800px_COLOURBOX22108014.jpg");
        }
    </style>

<div class="main">
    <header class="intro">
        <div class="container intro-content">
            <h1>"Pasha"</h1>
            <h3>Настоящий турецкий лукум стал еще ближе!</h3>
        </div>
    </header>
    <section class="pros container">
        <div>
            <div class="pros-image">
                <img src="<?=$PATH;?>/images/gears_2.svg" alt="">
            </div><h2>Лучшее оборудование</h2>
        </div>
        <div>
            <div class="pros-image">
                <img src="<?=$PATH;?>/images/chef.svg" alt="">
            </div><h2>Настоящий турецкий кондитер</h2>
        </div>
        <div>
            <div class="pros-image">
                <img src="<?=$PATH;?>/images/diamond.svg" alt="">
            </div><h2>Высочайшее качество</h2>
        </div>
        <div>
            <div class="pros-image">
                <img src="<?=$PATH;?>/images/wallet.svg" alt="">
            </div><h2>Привлекательная цена</h2>
        </div>
    </section>
    <section class="about-us container">
        <div class="about-us-text">
            <p>ООО "Эрсе" производит качественный, экологически чистый
                и просто очень вкусный рахат-лукум, используя турецкую
                рецептуру. Наше производство находится в Украине,
                поэтому цена намного ниже, чем в Турции. </p>

            <p>Продукция сертифицирована и производится без химических добавок.</p>
        </div>
        <div class="about-us-images">
            <div class="about-us-images-left">
                <div class="image-tall" style="margin-top: 30px; background-image: url(<?=$PATH;?>/images/loukum_granat.jpg)"></div>
                <div class="image-small" style="background-image: url(<?=$PATH;?>/images/loukum_color.jpg)"></div>
<!--                <img src="" alt="">-->
<!--                <img src="" alt="">-->
            </div>
            <div class="about-us-images-right">
                <div class="image-small" style="margin-bottom: 10px; background-image: url(<?=$PATH;?>/images/loukum_lemon.jpg)"></div>
                <div class="image-tall" style=" margin-bottom: 0; background-image: url(<?=$PATH;?>/images/IMG_7119.jpg)"></div>
<!--					<img src="" alt="">-->
<!--					<img src="" alt="">-->
				</div>
			</div>
		</section>
		<section class="cooperate-with-us">
			<div class="container cooperate-content">
				<h2>Попробуйте наш лукум "Pasha", и вы останетесь нашими клиентами навсегда!</h2>
			</div>
		</section>
		<section class="leaders">
			<div class="container leaders-content">
				<h3>Лидеры продаж</h3>
				<div>
                    <div class="leading-product">
                        <a class="leading-product-image" href="#">
                            <div style="background-image: url(<?=$PATH;?>/images/leader_1.jpg)"></div>
                        </a>
                        <a class="leading-product-name" href="#">Цветной</a>
                    </div>
                    <div class="leading-product">
                        <a class="leading-product-image" href="#">
                            <div style="background-image: url(<?=$PATH;?>/images/leader_2.jpg)"></div>
                        </a>
                        <a class="leading-product-name" href="#">Кокос</a>
                    </div>
                    <div class="leading-product">
                        <a class="leading-product-image" href="#">
                            <div style="background-image: url(<?=$PATH;?>/images/leader_3.jpg)"></div>
                        </a>

                        <a class="leading-product-name" href="#">Фрукты</a>
                    </div>
                    <div class="leading-product">
                        <a class="leading-product-image" href="#">
                            <div style="background-image: url(<?=$PATH;?>/images/leader_4.jpg)"></div>
                        </a>
                        <a class="leading-product-name" href="#">Киви</a>
                    </div>
				</div>
			</div>
		</section>
		<section class="news container">
			<h3>Новости</h3>
			<div class="latest-news">
				<div class="news-block">
                    <div class="news-image">
                        <a href="#">
                            <div style="background-image: url(<?=$PATH;?>/images/IMG_7095.jpg)"></div>
                        </a>
                    </div>
                    <div class="news-info">
                        <small>Новинка в каталоге</small>
                        <h3>Зимбабве</h3>
                        <p>Его состав: <br>
                            сахар, крахмал, вода, фруктовый сок, кокосовая стружка <br> </p>
                        <p>Упаковка 220гр, 400гр, 1.5кг</p>
                    </div>
                </div>
				<div class="news-block">
                    <div class="news-image">
                        <a href="#">
                            <div style="background-image: url(<?=$PATH;?>/images/IMG_7095.jpg)"></div>
                        </a>
                    </div>
                    <div class="news-info">
                        <small>Новинка в каталоге</small>
                        <h3>Зимбабве</h3>
                        <p>Его состав: <br>
                            сахар, крахмал, вода, фруктовый сок, кокосовая стружка <br> </p>
                        <p>Упаковка 220гр, 400гр, 1.5кг</p>
                    </div>
                </div>
				<div class="news-block">
                    <div class="news-image">
                        <a href="#">
                            <div style="background-image: url(<?=$PATH;?>/images/IMG_7095.jpg)"></div>
                        </a>
                    </div>
                    <div class="news-info">
                        <small>Новинка в каталоге</small>
                        <h3>Зимбабве</h3>
                        <p>Его состав: <br>
                            сахар, крахмал, вода, фруктовый сок, кокосовая стружка <br> </p>
                        <p>Упаковка 220гр, 400гр, 1.5кг</p>
                    </div>
                </div>
			</div>
		</section>
	</div>


<?php get_footer();

<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package pashalukum
 */

?>

</div>
<footer class="footer">
    <div class="container footer-content">
    </div>
</footer>
</div>

<?php wp_footer(); ?>

</body>
</html>
